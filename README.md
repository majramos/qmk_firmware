# Quantum Mechanical Keyboard Firmware (QMK)

This is a keyboard firmware based on the tmk_keyboard firmware with some useful features for Atmel AVR and ARM controllers, and more specifically, the OLKB product line, the ErgoDox EZ keyboard, and the Clueboard product line.

## Official Website / Documentation

[qmk.fm](https://qmk.fm) is the official website of QMK.

[docs.qmk.fm](https://docs.qmk.fm) is the official documentation.

[github.com/qmk/qmk_firmware](https://github.com/qmk/qmk_firmware) is the oficial github repo.

[config.qmk.fm](https://config.qmk.fm/) is the online configurator.


## QMK firmware

### extra stuff that may be required installing
when using docker container with a python image
```bash
apt install make gcc-avr avr-libc gcc-arm-none-eabi
```
`gcc-arm-none-eabi` required for compilling to liatris

### setup qmk
set keymap base name
```bash
qmk config user.keymap=majramos
```

### shortcut code for compiling
basic compile with default keymap, generate .hex
```bash
qmk compile -kb splitkb/kyria/rev3 -km default
```

#### when using *[LIATRIS](https://splitkb.com/products/liatris)*
- RP2040-based controller
- The Liatris uses the UF2 bootloader with .uf2 files
- Guide on flashing at [splitkb guides](https://docs.splitkb.com/hc/en-us/articles/6330981035676-Aurora-Build-Guide-20-Flashing-Firmware#h_01GGYW9WAZNTEWJMR7P17HM0T1)

compile with default keymap, generate .uf2
```bash
qmk compile -kb splitkb/kyria/rev3 -km default -e CONVERT_TO=liatris
```

## Kyria keyboard
- buy at [splitkb](https://splitkb.com/products/kyria-rev3-pcb-kit)
- qmk code location [kink](https://github.com/qmk/qmk_firmware/tree/master/keyboards/splitkb/kyria)

### Create from default
create new keymap (assumes keymap base name in config has been set previously)
```bash
qmk new-keymap -kb splitkb/kyria/rev3
```
new folder at `~/qmk_firmware/keyboards/splitkb/kyria/keymaps/majramos/`

make mirror folder at pyuser workspace
```bash
mkdir -p ~/workspace/keyboards/splitkb/kyria/keymaps/majramos
```

copy stuff to `~/workspace/keyboards/splitkb/kyria/keymaps/majramos/` to work on keymaps
```bash
cp -iR ~/qmk_firmware/keyboards/splitkb/kyria/keymaps/majramos/* ~/workspace/keyboards/splitkb/kyria/keymaps/majramos/
```

### Copy new changes
after making changes, copy stuff back to qmk_firmware directory
```bash
cp -iR ~/workspace/keyboards/splitkb/kyria/keymaps/majramos/* ~/qmk_firmware/keyboards/splitkb/kyria/keymaps/majramos/
```

compile `majramos` keymap to liatris
```bash
qmk compile -kb splitkb/kyria/rev3 -km majramos -e CONVERT_TO=liatris
```

copy .uf2 file to workspace
```bash
cp ~/qmk_firmware/splitkb_kyria_rev3_majramos_liatris.uf2 ~/workspace/splitkb_kyria_rev3_majramos_liatris.uf2
```

## References
- [keyboard-layout-editor](http://www.keyboard-layout-editor.com/)

- [precondition - A guide to home row mods](https://precondition.github.io/home-row-mods)
