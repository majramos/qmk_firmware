# Kyria's Majramos Keymap


The default keymap contains 3 layers.
Hardware features of the Kyria such as OLEDs, rotary encoders and underglow are also supported.

The five different layers are the following:
1. Base layer (QWERTY)
2. Symbols/Numbers layer
3. Function/Navigation layer


## Base layer(s)
```
Base Layer: QWERTY

,-------------------------------------------.                              ,-------------------------------------------.
|  Tab   |   Q  |   W  |   E  |   R  |   T  |                              |   Y  |   U  |   I  |   O  |   P  |  Bksp  |
|--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
| LShift |   A  |   S  |   D  |   F  |   G  |                              |   H  |   J  |   K  |   L  | ;  : |  '  "  |
|--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
| LCtrl  |   Z  |   X  |   C  |   V  |   B  | [ {  |CapsLk|  | RCtrl|  ] } |   N  |   M  | ,  < | . >  | /  ? | RShift |
`----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
                       |Encodr| LGUI | Enter|F-keys|      |  | Esc  | Sym  | Space| AltGr| Menu |
                       |VolMut|      |      | Nav  | LAlt |  |      |      |      |      |Encodr|
                       `----------------------------------'  `----------------------------------'
```

## Sym layer
```
Sym Layer: Numbers, symbols

,-------------------------------------------.                              ,-------------------------------------------.
|    `   |  1   |  2   |  3   |  4   |  5   |                              |   6  |  7   |  8   |  9   |  0   |   =    |
|--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
|    ~   |  !   |  @   |  #   |  $   |  %   |                              |   ^  |  &   |  *   |  (   |  )   |   +    |
|--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
|    |   |   \  |  :   |  ;   |  -   |  [   |  {   |NumLk |  |      |   }  |   ]  |  _   |  ,   |  .   |  /   |   ?    |
`----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
                       |      |      |      |      |      |  |      |      |      |      |      |
                       |      |      |      |      |      |  |      |      |      |      |      |
                       `----------------------------------'  `----------------------------------'
```


## Function/Navigation layer
```
Nav Layer: Media, navigation and function keys

,-------------------------------------------.                              ,-------------------------------------------.
|        |  F9  | F10  |  F11 |  F12 |      |                              | PgUp | Home |   ↑  | End  |Insert| Delete |
|--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
|        |  F5  |  Alt |  F7  |  F8  |      |                              | PgDn |  ←   |   ↓  |   →  | PrtSc|        |
|--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
|        |  F1  |  F2  |  F3  |  F4  |      | MOD  |ScLck |  | TOG  | RMOD | Pause|M Prev|M Play|M Next|      |        |
`----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
                       |      |      |      |      |      |  |      |      |      |      |      |
                       |      |      |      |      |      |  |      |      |      |      |      |
                       `----------------------------------'  `----------------------------------'
```


The <kbd>Alt</kbd> modifier, despite being situated on the right half of the keyboard is *not* `KC_RALT`, it is `KC_LALT`.
`KC_RALT` is actually the <kbd>AltGr</kbd> key which generally acts very differently to the left <kbd>Alt</kbd> key.
Keyboard shortcuts involving <kbd>AltGr</kbd>+<kbd>F#</kbd> are rare and infrequent as opposed to the much more common <kbd>Alt</kbd>+<kbd>F#</kbd> shortcuts.
Consequently, `KC_LALT` was chosen for the function layer.

Since there are more than 10 function keys, the cluster of F-keys does not follow the usual 3×3+1 numpad arrangement.


## Hardware Features

### Rotary Encoder
The left rotary encoder is programmed to control the volume.
The right encoder uses mouse keys and sends <kbd>WH_U</kbd> or <kbd>WH_D</kbd> on every turn.

more cool ideas for encoder at [mattir keymaps](https://github.com/qmk/qmk_firmware/tree/master/keyboards/splitkb/kyria/keymaps/mattir)

### OLEDs
The OLEDs display the current layer at the top of the active layers stack, the Kyria logo and lock status (caps lock, num lock, scroll lock).
